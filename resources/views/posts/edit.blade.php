@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
      <div class="card card-light mt-2">
        <div class="card-header">
          <h3 class="card-title">Buat Posting Baru</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{route('posts.update', $post->id)}}" method="POST">
          @method('patch')
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Konten</label>
              <textarea class="form-control" name="content" rows="3">{{$post->content}}</textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Sisipkan gambar</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="exampleInputFile">
                  <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text">Upload</span>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Posting</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection




