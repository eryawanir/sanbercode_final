@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
      @include('layouts.partials.wall')
    </div>
  </div>
</div>
@endsection