@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">


      <div class="col-md-5 mt-2 mx-auto">

        <!-- Profile Image -->
        <div class="card card-light card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-circle"
                   src="{{ url('/img/'.$user->profile->photo) }}"
                   alt="User profile picture">
            </div>

            <h3 class="profile-username text-center">{{$user->name}}</h3>

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>Followers</b> <a class="float-right">1,322</a>
              </li>
              <li class="list-group-item">
                <b>Following</b> <a class="float-right">543</a>
              </li>
            </ul>

            <a href="{{route('profile.edit',$user->profile->id)}}" class="btn btn-primary btn-block"><b>Edit Profil</b></a>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->

        <!-- About Me Box -->
        <div class="card card-light">
          <div class="card-header">
            <h3 class="card-title">About Me</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
           
            <strong><i class="far fa-file-alt mr-1"></i> Bio</strong>

            <p class="text-muted">{{$user->profile->bio}}</p>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->

    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
@endsection