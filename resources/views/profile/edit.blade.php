@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-9">
      <div class="card card-light mt-2">
        <div class="card-header">
          <h3 class="card-title">Edit Profil</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form action="{{route('profile.update', $profile->id)}}" method="POST" enctype="multipart/form-data">
          @method('patch')
          @csrf
          <div class="card-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Nama</label>
              <input name="name" type="text" class="form-control" id="exampleInputEmail1" 
              value="{{Auth::user()->name}}">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Bio</label>
              <textarea class="form-control" name="bio" rows="3">{{$profile->bio}}</textarea>
            </div>
            <div class="form-group">
              <label for="exampleInputFile">Gambar Profil</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" name="photo" id="exampleInputFile">
                  <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text">Upload</span>
                </div>
              </div>
            </div>
          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection




