<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')
            ->get();

        return view('posts.index', compact('posts'));
    }

    public function userposts()
    {
        $posts = Auth::user()->post->sortByDesc('created_at');
        $user = User::find(Auth::user()->id);
        return view('posts.userposts', compact('posts', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'content' => 'required',
        ]);

        Post::create([
            'content' => $request->content,
            'user_id' => Auth::user()->id
        ]);
        Alert::success('Berhasil di posting', 'cie bikin post baru');


        return redirect(route('posts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        if (!($post->user_id === Auth::user()->id)) {
            Alert::warning(
                'Gagal',
                'kamu dilarang mengupdatenya, kamu bukan pemilik post ini'
            );
            return redirect(route('userspost'));
        }

        $request->validate([
            'content' => 'required',
        ]);

        Post::where('id', $post->id)
            ->update([
                'content' => $request->content,
            ]);
        Alert::success('Berhasil diedit');


        return redirect(route('userspost'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (!($post->user_id === Auth::user()->id)) {
            Alert::warning(
                'Gagal',
                'kamu dilarang menghapusnya, kamu bukan pemilik post ini'
            );
            return redirect(route('userspost'));
        }
        $post = Post::find($post->id);
        $post->delete();
        Alert::success('Berhasil dihapus');


        return redirect(route('userspost'));
    }
}
