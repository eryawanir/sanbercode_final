# Final Project

## Kelompok 9  
## Anggota Kelompok
- Muhammad Eryawan  
- silahkan push namanya sendiri yaa
  
## Tema Projek
####  Bikin sosmed

Terdapat Beberapa fitur utama:

1. User harus terdaftar di web untuk menggunakan layanan sosmed
2. User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote.
3. User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri.
4. Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya.
5. Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan. Selain itu user bisa melakukan dislike.
6. Satu postingan dapat memiliki banyak komentar. Satu komentar dapat dikomentari oleh banyak user.
7. Satu komentar dapat disukai oleh banyak User. Satu User dapat menyukai banyak komentar.
8. Seorang User dapat membuat dan mengubah profile nya sendiri.
9. Pada halaman menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like.
10. Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)

## ERD nya

![alt text](https://gitlab.com/eryawanir/sanbercode_final/-/raw/main/public/img/erd.png)  

  
## Link video  
https://youtu.be/0m3XMcH8OQw
